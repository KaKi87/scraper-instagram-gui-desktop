#!/usr/bin/env bash
./node_modules/.bin/tauri signer generate -w ./$npm_package_name.key
PUBKEY=$(cat ./$npm_package_name.key.pub)
echo -E "$(jq --arg PUBKEY "$PUBKEY" '.tauri.updater.pubkey=$PUBKEY' ./src-tauri/tauri.conf.json)" > ./src-tauri/tauri.conf.json
rm ./$npm_package_name.key.pub