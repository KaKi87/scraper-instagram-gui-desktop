# `scraper-instagram-gui-desktop`

Desktop version of [`scraper-instagram-gui`](https://git.kaki87.net/KaKi87/scraper-instagram-gui) powered by [Tauri](https://tauri.app).

![](https://cdn.discordapp.com/attachments/723185786794803252/1035993551688962208/Screenshot_from_2022-10-29_17-36-19.png)